const TelegramBot = require('node-telegram-bot-api');
const { TOKEN } = require('./config');
const bot = new TelegramBot(TOKEN, {polling: true});

const { callbackHandler } = require('./handler/callbackHandler');
const { textHandler } = require('./handler/textHandler');

/**
 * /AKSES HANDLER UNTUK TYPE TEXT
 * disini semua message berupa text diproses
 */
bot.on('text', (msg) => {
	textHandler(msg, bot);
});

/**
 * /AKSES CALLBACK DARI INLINE BUTTON
 * dsisni semua callback dari inline keyboard diproses
 */
bot.on('callback_query', (callback) => {
	callbackHandler(callback, bot);
})

/**
 * /AKSES CALLBACK DARI INLINE BUTTON
 * dsisni semua callback dari inline keyboard diproses
 */
bot.on('document', (doc) => {
	const fs = require('fs');
	try {
	  fs.appendFileSync('./log.txt', `fileName: ${doc.document.file_name}\nfileID: ${doc.document.file_id}\n\n`);
	  console.log(`File ${doc.document.file_name} diterima`);
	} catch (err) {
	  console.log(error);
	}
})

/*

bot.onText(/sendfile/, (msg) => {
	bot.sendPhoto(msg.chat.id, 'http://www.makeindiaread.com/wp-content/uploads/2017/05/1-2.jpg');
	bot.sendDocument(msg.chat.id,'https://www.adobe.com/content/dam/acom/en/devnet/acrobat/pdfs/js_api_reference.pdf');
});
*/