/**
 * HOME BUTTON
 * button untuk halaman utam dan untuk back kalo gasalah
 * disini pakenya hex untuk emoticonya
 */
exports.homeButtonMarkup = {
	'reply_markup': {
	  'keyboard': [
	  	['\u{1F4DA} Daftar Buku', '\u{1F50E} Cari Buku', '\u{1F525} Hot Books'],
	  	['\u{1F4CC} Informasi', '\u{1F4EA} Saran & Pesan']
	  ],
	  resize_keyboard: true,
	},
	parse_mode: 'Markdown',
}

/**
 * BOOK LIST BUTTON
 * daftar button untuk kelas/genre buku
 */
exports.bookButtonMarkup = {
	'reply_markup': {
	  'keyboard': [
	  	['\u{1F525} Filsafat', '\u{1F4D9} Sastra', '\u{1F489} Kedokteran'],
	  	['\u{1F337} Agama', '\u{1F465} Psikologi', '\u{1F4BB} Komputer & programming'], 
	  	['\u{1F4E5} Download', '\u{1F466} Sosial', '\u{1F519} Back']
	  ],
	  resize_keyboard: true
	},
	parse_mode: 'Markdown',
}

/**
 * HOT BOOKS BUTTON
 * daftar button untuk buku2 pilihan
 */
exports.hotBooksButtonMarkup = {
	'reply_markup': {
	  'keyboard': [
	  	['\u{1F517} Pilihan Kami', '\u{1F4AF} Download Terbanyak'],
	  	['\u{1F559} Upload Terbaru', '\u{1F519} Back']
	  ],
	  resize_keyboard: true
	},
	parse_mode: 'Markdown',
}

/**
 * INFORMATION BUTTON
 * daftar button untuk menu informasi bot
 */
exports.informationButtonMarkup = {
	'reply_markup': {
	  'keyboard': [
	  	['\u{2753} FAQ', '\u{1F4E4} Kontribusi'],
	  	['\u{1F4C8} Statistik', '\u{1F519} Back']
	  ],
	  resize_keyboard: true
	},
	parse_mode: 'Markdown',
}