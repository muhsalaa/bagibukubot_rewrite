module.exports.MostDownloadedButton = (limit, data = [1,2,3,4,5,]) => { return { 
	reply_markup: {
		inline_keyboard: [[
			{text: limit[0], callback_data: `book1 ${data[0]}`},
			{text: limit[1], callback_data: `book2 ${data[1]}`},
			{text: limit[2], callback_data: `book3 ${data[2]}`},
			{text: limit[3], callback_data: `book4 ${data[3]}`},
			{text: limit[4], callback_data: `book5 ${data[4]}`},
		],[
			{text: '\u{23EA}', callback_data: 'first'},
			{text: '\u{25C0}', callback_data: 'prev'},
			{text: '\u{25B6}', callback_data: 'next'},
			{text: '\u{23E9}', callback_data: 'last'},
		]]
	},
	parse_mode: 'Markdown' 
}}